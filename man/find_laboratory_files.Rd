% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/parse.R
\name{find_laboratory_files}
\alias{find_laboratory_files}
\title{Find laboratory files}
\usage{
find_laboratory_files(initial_path)
}
\arguments{
\item{initial_path}{Caminho para buscar arquivos}
}
\description{
Dado um caminho local, busca por arquivos Excel
para interpretar como dados laboratoriais
}
