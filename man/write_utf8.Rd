% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{write_utf8}
\alias{write_utf8}
\title{write_utf8}
\usage{
write_utf8(text, con, ...)
}
\arguments{
\item{text}{String a exportar}

\item{con}{Conexão de saída}

\item{...}{Parâmetros adicionais à função writeLines}
}
\description{
Exporta string a um arquivo, forçando seu encoding a UTF-8
}
